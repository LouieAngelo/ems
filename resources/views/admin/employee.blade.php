@extends('layouts.admin')
@section('content')
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
           

 

            <div class="col-sm-12">

          @if ($message = Session::get('success')) 
          <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>	
          <strong>{{$message}}</strong>
          </div>
          @endif

     
              <ol class="breadcrumb  ">
                <li class="breadcrumb-item"><a href="#">Employee</a></li>
                <li class="breadcrumb-item active">List</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->

<div class="row">
    <div class="col-sm-12">
    <div class="card card-body">
        <div>
            <button class="btn btn-success btn-sm mb-2"  data-toggle="modal" data-target="#newEmployee"> <i class=" fa fa-plus"></i>  New Employee</button>
        </div>
        <table class="table table-striped projects customize-table table-sm">
            <thead class="bg-primary">
                <tr>
                    <th>Fullname</th>
                    <th>Company</th>
                    <th>Department</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
               @foreach ($employee as $employees)
               <tr>
                  <td>{{ $employees->fullname }}</td> 
                  <td>{{ $employees->company->name }}</td> 
                  <td>{{ $employees->department->department_name }}</td> 
                  <td>
                      <button class="btn-primary btn btn-sm edit_button" data-info="{{$employees->fullname}},{{$employees->id}}" title="Edit Record" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-edit  "></i></button>
                      <button class="btn-danger btn btn-sm delete_button" data-info="{{$employees->fullname}},{{$employees->id}}" title="Delete Record" data-toggle="tooltip"><i class="fa fa-trash "></i></button>
                    </td>
                </tr> 
               @endforeach
            </tbody>
      </table>
    </div>
    </div>

</div>


        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
  
@include('admin.modals.employee')
@endsection

@push('scripts')
 
<script>
 
$('.edit_button').on('click',function(){
 $name = $(this).data('info').split(',');
$('input[name=fullname]').val( $name[0] );
$('input[name=empId]').val( $name[1] );
$('#employeeUpdate').modal('show');
})


$('.delete_button').on('click',function(){
 $name = $(this).data('info').split(',');
document.getElementById('name').innerText = $name[0];
$('input[name=empId]').val( $name[1] );
$('#deleteId').modal('show');
})




// $('.submit_update').on('click',function(){
// var company = document.getElementById("company_id");
// var value = e.options[e.selectedIndex].value
//     alert( $('select[name=company]').val());
// });

</script>




@endpush