@extends('layouts.admin')
@section('content')
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
           

 

            <div class="col-sm-12">

          @if ($message = Session::get('success')) 
          <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>	
          <strong>{{$message}}</strong>
          </div>
          @endif

     
              <ol class="breadcrumb  ">
                <li class="breadcrumb-item"><a href="#">Company</a></li>
                <li class="breadcrumb-item active">List</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->

<div class="row">
    <div class="col-sm-12">
    <div class="card card-body">
        <div>
            <button class="btn btn-success btn-sm mb-2"  data-toggle="modal" data-target="#newCompany"> <i class=" fa fa-plus"></i>  New Record</button>
        </div>
         
        <table class="table table-striped projects customize-table table-sm">
            <thead class="bg-primary">
                <tr>
                    <th>Company id</th>
                    <th>Company Name</th>
                    <th>Create At</th>
                    <th>Update At</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
               @foreach ($company as $company)
               <tr>
                  <td>{{ $company->id }}</td> 
                  <td>{{ $company->name }}</td> 
                  <td>{{  date('d-m-Y', strtotime($company->created_at))}}</td> 
                  <td>{{  date('d-m-Y', strtotime($company->updated_at))}}</td> 
                  <td>
                      <button class="btn-primary btn btn-sm edit_company" data-info="{{$company->name}},{{$company->id}}" title="Edit Record" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-edit  "></i></button>
                      <button class="btn-danger btn btn-sm DeleteCompany"  data-info="{{$company->name}},{{$company->id}}"  title="Delete Record" data-toggle="tooltip"><i class="fa fa-trash "></i></button>
                  </td>
                </tr> 
               @endforeach
            </tbody>
      </table>
    
    </div>
    </div>

</div>


        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
  
@include('admin.modals.companies')
@endsection

@push('scripts')
 
<script>
 
$('.edit_company').on('click',function(){
$company = $(this).data('info').split(',');
$('input[name=companyName]').val( $company[0] );
$('input[name=compId]').val( $company[1] );
$('#updateCompany').modal('show');
}) 

$('.DeleteCompany').on('click',function(){
$company = $(this).data('info').split(',');
document.getElementById('name').innerText = $company[0];
$('input[name=empId]').val( $company[1] );
$('#deleteCompany').modal('show');
}) 
 
</script>




@endpush