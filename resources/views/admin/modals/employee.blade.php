  
 <!-- Modal -->
 <div class="modal fade" id="deleteId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
     <div class="modal-dialog" role="document">
         <div class="modal-content">
             <div class="modal-header">
                 <h5 class="modal-title">Delete Employee</h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
             </div>
             <form action="/deleteEmployee" method="post">
                {{ csrf_field() }}
             <div class="modal-body">
                Are you sure you want to delete?
                <br> <span class="text-danger text-capitalize" id="name"></span>
                <input type="text" class="form-control  "  style="display: none" name="empId">
             </div>
             <div class="modal-footer">
                 
                 <button type="submit" class="btn btn-danger">Delete</button>
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
             </div>
             </form>
         </div>
     </div>
 </div>


<!-- Modal -->
<div class="modal fade" id="employeeUpdate" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <form action="/updateEmployee" method="post">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="modal-body">
                            <div class="container-fluid">
                             <div class="form-group row">
                                <label for="fullname" class="col-sm-3 col-form-label">Name</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control"  name="fullname">
                                  <input type="text" class="form-control  "  style="display: none" name="empId">
                                </div>
                                
                             </div>
                             <div class="form-group row">
                              
                                <label for="Company" class="col-sm-3 col-form-label">Company</label>
                                <div class="col-sm-9">
                                    <select class="form-control " name="company" >
                                        @foreach ($company as $companies)
                                        <option value="{{ $companies->id }}">{{ $companies->name }}</option> 
                                        @endforeach
                                      </select>
                                </div>
            
                             </div>
                             <div class="form-group row">
                              
                                <label for="fullname" class="col-sm-3 col-form-label">Department</label>
                                <div class="col-sm-9">
                                <select class="form-control " name="department" >
                                    @foreach ($department as $dept)
                                    <option value="{{ $dept->id }}">{{ $dept->company->name }} {{ $dept->department_name }}</option> 

                                    @endforeach
                                </select>
                                </div>
            
                             </div>
                            </div>
                        </div>
         
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
            <button type="" class="btn btn-secondary" data-dismiss="modal">Close</button>
               
            </div>
        </div>
    </div>
</div> 



<!-- Modal -->
<div class="modal fade" id="newEmployee" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">New Employee</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>

        <form action="/newEmployee" method="post">
            {{ csrf_field() }}
            <div class="modal-body">
                <div class="modal-body">
                    <div class="container-fluid">
                     <div class="form-group row">
                        <label for="fullname" class="col-sm-3 col-form-label">Name</label>
                        <div class="col-sm-9">
                          <input type="text" class="form-control"  name="fullname">
                        </div>
                        
                     </div>
                     <div class="form-group row">
                      
                        <label for="Company" class="col-sm-3 col-form-label">Company</label>
                        <div class="col-sm-9">
                            <select class="form-control " name="company" id="company_id">
                                @foreach ($company as $companies)
                                <option value="{{ $companies->id }}">{{ $companies->name }}</option> 
                                @endforeach
                              </select>
                        </div>
    
                     </div>
                     <div class="form-group row">
                      
                        <label for="fullname" class="col-sm-3 col-form-label">Department</label>
                        <div class="col-sm-9">
                            <select class="form-control " name="department" id="company_id">
                                @foreach ($department as $department)
                                <option value="{{ $department->id }}">{{ $department->company->name }} - {{ $department->department_name }}</option> 
                                @endforeach
                              </select>
                        </div>
    
                     </div>
                    </div>
                </div>
 
            </div>
            <div class="modal-footer">
          
                <button type="submit" class="btn btn-success " >Create</button>
            </form>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>





 



<script>
    $('#exampleModal').on('show.bs.modal', event => {
        var button = $(event.relatedTarget);
        var modal = $(this);
    
    });
</script>