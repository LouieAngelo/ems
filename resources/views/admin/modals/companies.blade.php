   
 <!-- Modal -->
 <div class="modal fade" id="deleteCompany" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Delete Employee</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <form action="/deleteCompany" method="post">
               {{ csrf_field() }}
            <div class="modal-body">
               Are you sure you want to delete?
               <br> <span class="text-danger text-capitalize" id="name"></span>
               <input type="text" class="form-control  "  style="display: none" name="empId">
 
            </div>
            <div class="modal-footer">
                
                <button type="submit" class="btn btn-danger">Delete</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="newCompany" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="/newCompany" method="post">
                {{ csrf_field() }}
            <div class="modal-header">
                <h5 class="modal-title">New Company Record</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="form-group row">
                       <label for="company_Name" class="col-sm-3 col-form-label text-capitalize">Company:</label>
                       <div class="col-sm-9">
                         <input type="text" class="form-control"  name="companyName">
                         
                        </div>
                       
                    </div>
        
                   </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Save</button>
            </form>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
               
            </div>
        </div>
    </div>
</div>



 

<!-- Modal -->
<div class="modal fade" id="updateCompany" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="/updateCompany" method="post" >
            {{ csrf_field() }}
            <div class="modal-header">
                <h5 class="modal-title">New Company Record</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="form-group row">
                       <label for="company_Name" class="col-sm-3 col-form-label text-capitalize">Company:</label>
                       <div class="col-sm-9">
                         <input type="text" class="form-control"  name="companyName">
                         <input type="text" class="form-control" style="display: none"  name="compId">
                        </div>
                       
                    </div>
        
                   </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Update</button>
            </form>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
               
            </div>
        </div>
    </div>
</div>