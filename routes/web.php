<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 

// Route::get('/','EmployeeCrudeController@index');
Route::get('/Dashboard','EmployeeCrudeController@index')->name('dashboard');
Route::get('/EmployeeList','EmployeeCrudeController@employeeList')->name('employee_list');
Route::get('/CompanyList','CompanyController@companyList');
Route::get('/DepartmentList','DepartmentController@index');


Route::post('/newEmployee','EmployeeCrudeController@create_employee')->name('employee.create');
Route::post('/updateEmployee','EmployeeCrudeController@edit')->name('employee.update');
Route::post('/deleteEmployee','EmployeeCrudeController@destroy')->name('employee.delete');

Route::post('/newCompany','CompanyController@insert')->name('company.create');
Route::post('/updateCompany','CompanyController@edit')->name('company.update');
Route::post('/deleteCompany','CompanyController@delete')->name('company.delete');


// Route::get('/CompanyDepartment','HomeController@departmentList');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
