<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    protected $fillable = ['name','create_at','updated_at'];
    protected $table ="company";

    public function employees(){
        return $this->hasMany(Employee::class);
    }
  
    public function department(){
        return $this->hasMany(Department::class,'companies_id');
    }

}
