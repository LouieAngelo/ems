<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
  protected $fillable = ['companies_id','department_name','create_at','update_at'];
    protected $table = "departments";
    
    public function employees(){
        return $this->hasMany(Employee::class);
    }

    public function company(){
      return $this->belongsTo(Company::class,'companies_id');
    }
    

}
