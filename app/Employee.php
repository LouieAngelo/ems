<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['companies_id','departments_id','fullname','created_at','updated_at'];
    protected $primaryKey = 'id';
    protected $table ="employees";
    
    public function company(){
        return $this->belongsTo(Company::class,'companies_id');
    }

    public function department(){
        return $this->belongsTo(Department::class,'departments_id');
    }
}
