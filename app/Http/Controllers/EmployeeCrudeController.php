<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Company;
use App\department;
use Carbon\carbon;
class EmployeeCrudeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      return view('admin.dashboard');
    }

public function employeeList()
{ 
     $employee = Employee::all();
     $company = Company::all();
     $department = Department::all();
     return view('admin.employee',compact('employee','company','department'));
}



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_employee(Request $req)
    {
       $data = [
           'companies_id'=>$req->company,
           'departments_id'=>$req->department,
           'fullname'=>$req->fullname,
           'created_at'=>Carbon::now(),
           'updated_at'=>Carbon::now(),
       ];

       Employee::create($data);
       return redirect()->back()->with('success', 'Successfully Recorded');   
        // return response()->ajax(['success'=>"successfully recorded"]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
       


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $req)
    {
        $query = Employee::find($req->empId);
        $query->companies_id = $req->company;
        $query->departments_id = $req->department;
        $query->fullname=$req->fullname;
        $query->updated_at=Carbon::now();
        $query->save();
        return redirect()->back()->with('success', 'Successfully Updated');   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req)
    {
        $query = Employee::find($req->empId);
        $query->delete();
        return redirect()->back()->with('success', 'Successfully Deleted');   
    }
}
