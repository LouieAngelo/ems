<?php

namespace App\Http\Controllers;
use App\Employee;
use App\Company;
use App\department;
use Carbon\carbon;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
 
    public function index(){
        $department = Department::all();
        return view('admin.department',compact('department'));
    }
}
