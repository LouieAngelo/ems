<?php

namespace App\Http\Controllers;
use App\Employee;
use App\Company;
use App\department;
use Carbon\carbon;
use Illuminate\Http\Request;

class CompanyController extends Controller
{

    public function companyList()
    { 
         $company = Company::all();
         return view('admin.company',compact('company'));
         
    }

    public function insert(Request $request){

        $data = [
            'name'=> $request->companyName,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
                ];
          Company::create($data);
        return redirect()->back()->with('success', $request->company_Name. ' Successfully Recorded');   
    }

    public function edit(Request $request){

        $query = Company::find($request->compId);
        $query->name = $request->companyName;
        $query->updated_at=Carbon::now();
        $query->save();

        return redirect()->back()->with('success', $request->company_Name. ' Successfully Updated');  
    }

    public function delete(Request $request){
        $delete = Company::find($request->empId)->delete();
        return redirect()->back()->with('success', ' Successfully Deleted');  
    }



}
